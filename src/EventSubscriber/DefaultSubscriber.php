<?php

namespace Drupal\m8_ga_missing_tracking\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class DefaultSubscriber.
 *
 * @package Drupal\ga_missing_tracking
 */
class DefaultSubscriber implements EventSubscriberInterface {

  /**
   * Has the GA tracking id been entered?
   *
   * @var Bool $id_exists
   */
  protected $id_exists;

  /**
   * The URL of the GA settings form.
   *
   * @var Drupal\Core\Url $id_url
   */
  protected $id_url;

  /**
   * Constructs a new DefaultSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->id_exists = $config_factory->get('google_analytics.settings')->get('account');
    $this->id_url = Url::fromRoute('google_analytics.admin_settings_form');
  }

  /**
   * {@inheritdoc}
   */
  public function checkForGaId(GetResponseEvent $event) {
    if (!$this->id_exists) {
      \Drupal::messenger()->addMessage(Link::fromTextAndUrl(t('Please enter a Google Analytics tracking ID.'), $this->id_url), 'error');
    }
    else {
      \Drupal::messenger()->addMessage(t('Please disable the Google Analytics Missing Tracking Code module since you have already entered a Google Analytics tracking ID for this website.'), 'warning');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForGaId');
    return $events;
  }

}
