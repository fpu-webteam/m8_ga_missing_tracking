# Google Analytics Missing Tracking Code

This module will provide a status message if a site has the Google Analytics module installed and enabled, but has not yet entered a GA tracking ID.

Using the EventSubscriberInterface, the module will check if the GA tracking ID exists in configuration on each request (but not overriding cache), simililarly to hook\_init() in Drupal 7. If the tracking ID **does not exist** in config, a status message will display requesting the user to enter a GA tracking ID. If the tracking ID **does exist**, a status message will display requesting the user to disable the module since it is no longer useful.

## Authors

* FPU Webteam

## Acknowledgments

* FPU's friendliest web developer
